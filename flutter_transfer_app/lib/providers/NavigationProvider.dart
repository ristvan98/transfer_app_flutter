import 'package:flutter/foundation.dart';

class NavigationProvider extends ChangeNotifier{
  String screen = '/';

  void changeScreen(String newScreen){
    screen = newScreen;
    notifyListeners();
  }
}