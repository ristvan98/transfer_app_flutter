import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_transfer_app/models/User.dart';
import 'package:http/http.dart' as http;
enum Status {
  NotLoggedIn,
  LoggedIn,
  LoggedOut
}
class UserProvider extends ChangeNotifier {
  var loggedUser;
  Status _loggedInStatus = Status.NotLoggedIn;

  Status get loggedInStatus => _loggedInStatus;

  User get LoggedUser => loggedUser;

  void setloggedInStatus(Status val) {
    _loggedInStatus = val;
    notifyListeners();
  }

  void setLoggedUser(User user) {
    loggedUser = user;
    notifyListeners();
  }

  Future<Map<String, dynamic>> login(String username, String password) async {
    var result;
    var url = "http://192.168.1.6/php/signin.php";

    var response = await http.post(Uri.parse(url),
        body: {'username': username, 'password': password});
    if (response.statusCode == 200) {
      var responsedata = json.decode(response.body);
      if (responsedata["success"]) {
        User loggedUser = User(id: responsedata["uid"],
            username: responsedata["username"],
            password: responsedata["password"],
            image: responsedata["image"]);
        _loggedInStatus = Status.LoggedIn;
        notifyListeners();
        result = {'status': true, 'msg': 'Sikeres', 'user': loggedUser};
      } else {
        _loggedInStatus = Status.NotLoggedIn;
        notifyListeners();
        result = {'status': false, 'msg': responsedata["message"].toString()};
      }
    }
    return result;
  }

  Future<String> register(String username, String password) async {
    var url = "http://192.168.1.6/php/signup.php";
    String result = "";
    var response = await http.post(
        Uri.parse(url), body: {'username': username, 'password': password});
    if (response.statusCode == 200) {
      result = json.decode(response.body);
    }
    return result;
  }

  Future<void> update(User user) async {
    var url = "http://192.168.1.6/php/update.php";
    await http.post(Uri.parse(url), body: {
      'username': user.username,
      'password': user.password,
      'id': user.id
    });
    notifyListeners();
  }
}