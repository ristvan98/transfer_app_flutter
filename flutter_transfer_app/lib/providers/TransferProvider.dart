import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_transfer_app/models/Reservation.dart';
import 'package:flutter_transfer_app/models/Transfer.dart';
import 'package:http/http.dart' as http;

class TransferProvider extends ChangeNotifier{

  var transfer;

  Transfer get choose => transfer;

  void setTransfer(Transfer t){
    transfer = t;
    notifyListeners();
  }

  static Future<List<Transfer>> loadTransfers() async {
    var transfers;
    var url = "http://192.168.1.6/php/transfer.php";
    var response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      transfers = (json.decode(response.body) as List)
          .map((e) => Transfer.fromJson(e))
          .toList();
      //notifyListeners();
    }
    return transfers;
  }

  Future<void> saveReservation(Reservation reservation) async {
    var url = "http://192.168.1.6/php/reservation.php";
    await http.post(Uri.parse(url),body: {'transfer_id': reservation.transferId.toString(), 'user_id': reservation.userId.toString(), 'megjegyzes': reservation.megjegyzes});
    notifyListeners();
  }
}