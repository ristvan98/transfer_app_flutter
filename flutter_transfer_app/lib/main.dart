import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/localization/Localization.dart';
import 'package:flutter_transfer_app/providers/NavigationProvider.dart';
import 'package:flutter_transfer_app/providers/TransferProvider.dart';
import 'package:flutter_transfer_app/providers/UserProvider.dart';
import 'package:flutter_transfer_app/screens/HomeScreen.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_transfer_app/screens/LoginScreen.dart';
import 'package:flutter_transfer_app/screens/ProfileScreen.dart';
import 'package:flutter_transfer_app/screens/RegisterScreen.dart';
import 'package:provider/provider.dart';

import 'screens/TransferScreen.dart';

Future<void> main(List<String> args) async {
  WidgetsFlutterBinding.ensureInitialized();
  final cameras = await availableCameras();
  final camera = cameras.length > 0 ? cameras.first : null;
  runApp(
    MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) => NavigationProvider()),
      ChangeNotifierProvider(create: (_) => TransferProvider()),
      ChangeNotifierProvider(create: (_) => UserProvider()),
      Provider.value(value: camera),
    ],
      child: const MyApp(),
    )
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var _navigation = context.watch<NavigationProvider>();
    var _transferProvider = context.read<TransferProvider>();
    var _userProvider = context.read<UserProvider>();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Colors.redAccent.shade700,
      ),
      home: Navigator(
        pages: [
          (_userProvider.loggedInStatus == Status.LoggedIn) ? MaterialPage(child: HomeScreen()) : MaterialPage(child: LoginScreen()),
          if(_navigation.screen == "/transfer")
            MaterialPage(child: TransferScreen(transfer: _transferProvider.choose)),
          if(_navigation.screen == "/register")
            MaterialPage(child: RegisterScreen()),
          if(_navigation.screen == "/profile")
            MaterialPage(child: ProfileScreen()),
        ],
        onPopPage: (route, result){
          if(!route.didPop(result)) return false;
          _navigation.changeScreen("/");
          return true;
        },
      ),
      localizationsDelegates: [
        TransferAppLocalization.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en',''),
        const Locale('hu',''),
      ],
    );
  }
}

