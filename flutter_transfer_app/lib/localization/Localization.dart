import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'hu.dart';
import 'en.dart';

class TransferAppLocalization{
  final Locale locale;

  TransferAppLocalization(this.locale);


  static TransferAppLocalization? of(BuildContext context){
   return Localizations.of<TransferAppLocalization>(context, TransferAppLocalization);
  }

  static const LocalizationsDelegate<TransferAppLocalization> delegate = _TransferAppLocalizationDelegate();
  static Map<String, Map<String, String>> _localizedValues = {
    'en': ENGLISH_TEXTS,
    'hu': HUNGARIAN_TEXTS,
  };

  String stringById(String id) =>
      _localizedValues[locale.languageCode]?[id] ??
          'Missing translation: $id for locale: ${locale.languageCode}';

  String get homeScreenTitle => stringById('homeScreenTitle');
  String get transferDetailScreenTitle => stringById("transferDetailScreenTitle");
  String get wifi => stringById('wifi');
  String get legkondi => stringById('legkondi');
  String get bankkartya => stringById('bankkartya');
  String get foglalas => stringById('foglalas');
  String get megjegyzes => stringById('megjegyzes');
  String get megse => stringById('megse');
  String get text =>stringById('text');
  String get belepes => stringById('belepes');
  String get bejelentkezes => stringById('bejelentkezes');
  String get felhasznalonev => stringById('felhasznalonev');
  String get jelszo => stringById('jelszo');
  String get nincsfiok => stringById('nincsfiok');
  String get regisztralj => stringById('regisztralj');
  String get regisztracio => stringById('regisztracio');
  String get kuldes => stringById('kuldes');
  String get vanfiok => stringById('vanfiok');
  String get jelentkezzbe => stringById('jelentkezzbe');
  String get kotelezomezo => stringById('kotelezomezo');
  String get fotoKeszites => stringById('fotoKeszites');
  String get profil => stringById('profil');
  String get nev => stringById('nev');
  String get pw => stringById('pw');
  String get mentes => stringById('mentes');
}



class _TransferAppLocalizationDelegate
    extends LocalizationsDelegate<TransferAppLocalization> {
  const _TransferAppLocalizationDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'hu'].contains(locale.languageCode);

  @override
  Future<TransferAppLocalization> load(Locale locale) {
    return SynchronousFuture<TransferAppLocalization>(
      TransferAppLocalization(locale),
    );
  }

  @override
  bool shouldReload(_TransferAppLocalizationDelegate old) => false;
}