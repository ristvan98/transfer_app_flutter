import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/localization/Localization.dart';
import 'package:flutter_transfer_app/models/Transfer.dart';
import 'package:flutter_transfer_app/providers/NavigationProvider.dart';
import 'package:flutter_transfer_app/providers/TransferProvider.dart';
import 'package:flutter_transfer_app/providers/UserProvider.dart';
import 'package:provider/provider.dart';

import 'home/ShimmerWidget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late TransferProvider provider;
  late NavigationProvider navigation;
  late UserProvider userProvider;
  List<Transfer> transfer = [];
  bool isLoading = false;
  @override
  void initState(){
    super.initState();
    provider = context.read<TransferProvider>();
    navigation = context.read<NavigationProvider>();
    userProvider = context.read<UserProvider>();
    load();
  }

  Future load() async{
    setState(() {
      isLoading = true;
    });
    var data = await TransferProvider.loadTransfers();
    await Future.delayed(Duration(seconds: 2),() {});
    setState(() {
      transfer = data;
    });

    setState(() {
      isLoading = false;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(TransferAppLocalization.of(context)!.homeScreenTitle),
        backgroundColor: Colors.redAccent.shade700,
        shadowColor: Colors.redAccent.shade700,
        centerTitle: true,
        actions: [
          ButtonTheme(
            padding: EdgeInsets.all(0),
            minWidth: 30,
            child: FlatButton(
              onPressed: (){
                userProvider.setloggedInStatus(Status.LoggedOut);
                navigation.changeScreen("/");
              },
              child: Icon(Icons.logout,
                  size: 25, color: Colors.white),
            ),
          ),
        ],
        leading: ButtonTheme(
          padding: EdgeInsets.all(0),
          minWidth: 30,
          child: FlatButton(
            onPressed: (){
              navigation.changeScreen("/profile");
            },
            child: Icon(Icons.person,
                size: 30, color: Colors.white),
          ),
        ),
      ),
      body: ListView.builder(itemBuilder: (context, index){if(isLoading){return buildTransferShimmer();}else{return buildTransfer(transfer[index]);}}, itemCount: isLoading ? 5 : transfer.length,)
    );
  }
  Widget buildTransferShimmer() => ListTile(
    leading: ShimmerWidget.circular(width: 64, height: 64),
    title: ShimmerWidget.rectangular(height: 16),
    subtitle: ShimmerWidget.rectangular(height: 14),
  );
  Widget buildTransfer(Transfer transfer) => ListTile(
    leading: CircleAvatar(
      radius: 32,
      backgroundImage: NetworkImage(transfer.image),
    ),
    title: Text(transfer.hova, style: TextStyle(fontSize: 16),),
    subtitle: Text(('${transfer.ar} Ft'), style: TextStyle(fontSize: 14),maxLines: 1,),
    onTap: (){
      print("tap");
      navigation.changeScreen("/transfer");
      provider.setTransfer(transfer);
    },
  );

}
