import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/localization/Localization.dart';
import 'package:flutter_transfer_app/providers/UserProvider.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late GlobalKey<FormState> formKey;
  late TextEditingController _username;
  late TextEditingController _password;

  @override
  void initState(){
    super.initState();
    formKey = GlobalKey<FormState>();
    _username = TextEditingController();
    _password = TextEditingController();
  }

  Future<void> doRegister() async {
    if(formKey.currentState!.validate()){
      formKey.currentState!.save();
      var response = await Provider.of<UserProvider>(context, listen: false).register(_username.text, _password.text);
      if(response != "Sikeres regisztráció!"){
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(backgroundColor: Colors.red.shade900,
            content: Row(children: [Icon(Icons.warning, color: Colors.white,),SizedBox(width: 20,), Expanded(child: Text(response.toString()),)],)
        ));
      }else{
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(backgroundColor: Colors.green.shade900,
            content: Row(children: [Icon(Icons.check, color: Colors.white,),SizedBox(width: 20,), Expanded(child: Text(response.toString()),)],)
        ));
        _username.text = "";
        _password.text = "";
      }
    }else{
      print("not validated");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        title: Text(TransferAppLocalization.of(context)!.regisztracio),
        backgroundColor: Colors.redAccent.shade700,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.all(36),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    validator: (value){
                      if(value == ""){
                        return TransferAppLocalization.of(context)!.kotelezomezo;
                      }else{
                        return null;
                      }
                    },
                    controller: _username,
                    obscureText: false,
                    decoration: InputDecoration(
                      labelText: TransferAppLocalization.of(context)!.felhasznalonev,
                    ),
                  ),
                  SizedBox(height: 25.0),
                  TextFormField(
                    validator: (value){
                      if(value == ""){
                        return TransferAppLocalization.of(context)!.kotelezomezo;
                      }else{
                        return null;
                      }
                    },
                    controller: _password,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: TransferAppLocalization.of(context)!.jelszo,
                    ),
                  ),
                  SizedBox(
                    height: 35.0,
                  ),
                  Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(30.0),
                    color: Colors.redAccent.shade700,
                    child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () {
                        doRegister();
                      },
                      child: Text(TransferAppLocalization.of(context)!.kuldes,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white)),
                    ),
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(TransferAppLocalization.of(context)!.vanfiok),
                      SizedBox(
                        width: 5.0,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(TransferAppLocalization.of(context)!.jelentkezzbe,
                            style: TextStyle(color: Colors.redAccent.shade700)),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
