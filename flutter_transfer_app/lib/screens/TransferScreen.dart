import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/localization/Localization.dart';
import 'package:flutter_transfer_app/screens/transfer/SlidingPanel.dart';
import 'package:provider/provider.dart';

import '../models/Transfer.dart';


class TransferScreen extends StatefulWidget {
  final Transfer transfer;
  const TransferScreen({Key? key, required this.transfer}) : super(key: key);

  @override
  _TransferScreenState createState() => _TransferScreenState();
}

class _TransferScreenState extends State<TransferScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(TransferAppLocalization.of(context)!.transferDetailScreenTitle),
        backgroundColor: Colors.redAccent.shade700,
        shadowColor: Colors.redAccent.shade700,
      ),
      body: SlidingPanel(transfer: widget.transfer)
    );
  }

}

