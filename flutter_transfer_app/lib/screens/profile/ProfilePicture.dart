import 'dart:io';
import 'package:flutter_transfer_app/providers/NavigationProvider.dart';
import 'package:flutter_transfer_app/providers/UserProvider.dart';
import 'package:http/http.dart' as http;
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'CameraPage.dart';

class ProfilePicture extends StatefulWidget {
  final String img;
  const ProfilePicture({Key? key, required this.img}) : super(key: key);

  @override
  _ProfilePictureState createState() => _ProfilePictureState();
}

class _ProfilePictureState extends State<ProfilePicture> {
  late List<CameraDescription> cameras;
  late CameraDescription camera;

  String? imgName;
  void setImageName(String name){
    setState(() {
      imgName = name;
    });
  }
  
  @override
  void initState(){
    super.initState();
    availableCameras().then((availableCameras){
      cameras = availableCameras;
      camera = cameras.first;
    });
    setImageName(widget.img.toString());
  }

  Future uploadImage(String path) async {
    var uri = Uri.parse("http://192.168.1.6/php/upload.php");
    var request = http.MultipartRequest("POST",uri);
    request.fields["user_id"] = Provider.of<UserProvider>(context, listen: false).LoggedUser.id;
    var pic = await http.MultipartFile.fromPath("image", path);
    request.files.add(pic);
    var response = await request.send();
    if(response.statusCode == 200){
      print("image uploaded");

    }else{
      print("image not uploaded");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(150),
            color: Colors.white,
          ),
          child: CircleAvatar(
            radius: 70,
            backgroundColor: Colors.white,
            backgroundImage: widget.img != "" ? imgName == "" ? NetworkImage("http://192.168.1.6/php/uploads/${widget.img.toString()}") : NetworkImage("http://192.168.1.6/php/uploads/${imgName.toString()}") : imgName == "" || widget.img == "" ? NetworkImage("http://192.168.1.6/php/uploads/avatar.jpg") : NetworkImage("http://192.168.1.6/php/uploads/${imgName.toString()}"),
          ),
        ),
        Positioned(
          child: GestureDetector(
            child: Icon(
              Icons.photo_camera,
              size: 28,
              color: Colors.red.shade900,
            ),
            onTap: () async {
               XFile newImage = await Navigator.push(
                 context,
                 MaterialPageRoute(
                   builder: (context) => CameraPage(),
                 ),
               );
               if(newImage != null){
                 uploadImage(newImage.path);
                 setImageName(newImage.name);
               }
            }
          ),
          right: 0,
          bottom: 0,
        ),
      ],
    );
  }
}
