import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/localization/Localization.dart';
import 'package:flutter_transfer_app/providers/UserProvider.dart';
import '../../models/User.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class ProfileForm extends StatefulWidget {
  final User user;
  const ProfileForm({Key? key, required this.user}) : super(key: key);

  @override
  _ProfileFormState createState() => _ProfileFormState();
}

class _ProfileFormState extends State<ProfileForm> {
  final _formKey = GlobalKey<FormState>();
  var data;

  late TextEditingController _usernameController;
  late TextEditingController _passwordController;
  String? name;
  String? pw;

  @override
  void initState() {
    super.initState();
    setState(() {
      name = widget.user.username;
    });
    setState(() {
      pw = widget.user.password;
    });
    _usernameController = TextEditingController.fromValue(
        TextEditingValue(text: name.toString()));
    _passwordController = TextEditingController.fromValue(
        TextEditingValue(text: pw.toString()));
  }

  void saveProfile(User user) async {
    var userProvider = context.read<UserProvider>();
    await userProvider.update(user);
    userProvider.notifyListeners();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(backgroundColor: Colors.green.shade900,
        content: Row(children: [Icon(Icons.check, color: Colors.white,),SizedBox(width: 20,), Expanded(child: Text('Sikeres módosítás!'),)],)
    ));
  }
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        child: ListView(
          children: <Widget>[
            SizedBox(height: 40),
            Card(
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                  children: [
                    TextFormField(
                      controller: _usernameController,
                      decoration: InputDecoration(
                        labelText: TransferAppLocalization.of(context)!.nev,
                      ),
                      validator: (value) {
                        if (value != null && value.isEmpty) {
                          return TransferAppLocalization.of(context)!.kotelezomezo;
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _passwordController,
                      decoration: InputDecoration(
                        labelText: TransferAppLocalization.of(context)!.pw,
                      ),
                      validator: (value) {
                        if (value != null && value.isEmpty) {
                          return TransferAppLocalization.of(context)!.kotelezomezo;
                        }
                        return null;
                      },
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: RaisedButton(
                        color: Colors.red,
                        textColor: Colors.white,
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            User newUser = User(id: widget.user.id, username: _usernameController.text, password: _passwordController.text, image: widget.user.image);
                            setState(() {
                              name = newUser.username;
                              pw = newUser.password;
                            });
                            saveProfile(newUser);
                          }
                        },
                        child: Text(TransferAppLocalization.of(context)!.mentes),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
