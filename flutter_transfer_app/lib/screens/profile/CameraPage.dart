import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/localization/Localization.dart';
import 'package:flutter_transfer_app/providers/NavigationProvider.dart';
import 'package:flutter_transfer_app/providers/UserProvider.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../../CameraManager.dart';



class CameraPage extends StatelessWidget {
  const CameraPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
  var camera = context.watch<CameraDescription>();
  if(camera == null){
  return CircularProgressIndicator();
  }
  return TakePicture(camera: camera);
  }
}


class TakePicture extends StatefulWidget {
  final CameraDescription camera;
  const TakePicture({Key? key, required this.camera}) : super(key: key);

  @override
  _TakePictureState createState() => _TakePictureState();
}

class _TakePictureState extends State<TakePicture> {
  late Future<void> controller;
  late CameraManager cameraManager;

  void initState() {
    super.initState();
    cameraManager = CameraManager(camera: widget.camera);
    controller = cameraManager.initialize();
  }

  @override
  void dispose() {
    cameraManager.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(TransferAppLocalization.of(context)!.fotoKeszites),
        backgroundColor: Colors.redAccent.shade700,
        shadowColor: Colors.redAccent.shade700,
      ),
      body: FutureBuilder(
        future: controller,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Container(height: MediaQuery.of(context).size.height, child: CameraPreview(cameraManager.cameraController),);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.redAccent.shade700,
        child: Icon(Icons.camera_alt),
        onPressed: () async {
          try {
            await controller;
            final picture = await cameraManager.cameraController.takePicture();
            Navigator.pop(context, picture);
          } catch (e) {
            print(e);
          }
        },
      ),
    );
  }
}
