import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/localization/Localization.dart';
import 'package:flutter_transfer_app/models/Reservation.dart';
import 'package:flutter_transfer_app/models/Transfer.dart';
import 'package:flutter_transfer_app/providers/TransferProvider.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'TransferFormDialog.dart';

class PanelWidget extends StatefulWidget {
  final ScrollController controller;
  final PanelController panelcontroller;
  final Transfer transfer;
  const PanelWidget(
      {Key? key,
      required this.controller,
      required this.panelcontroller,
      required this.transfer})
      : super(key: key);

  @override
  State<PanelWidget> createState() => _PanelWidgetState();
}

class _PanelWidgetState extends State<PanelWidget> {

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      controller: widget.controller,
      children: <Widget>[
        SizedBox(
          height: 12,
        ),
        GestureDetector(
          child: Center(
            child: Container(
              width: 40,
              height: 10,
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(12),
              ),
            ),
          ),
          onTap: () {
            if (widget.panelcontroller.isPanelOpen) {
              widget.panelcontroller.close();
            } else {
              widget.panelcontroller.open();
            }
          },
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                TransferAppLocalization.of(context)!.transferDetailScreenTitle,
                style: TextStyle(fontSize: 24),
              ),
            ],
          ),
        ),
        SizedBox(height: 40),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              children: <Widget>[
                Icon(Icons.wifi,color: Colors.orange,size: 40,),
                Text(TransferAppLocalization.of(context)!.wifi),
              ],
            ),
            Column(
              children: <Widget>[
                Icon(Icons.air,color: Colors.blue ,size: 40,),
                Text(TransferAppLocalization.of(context)!.legkondi),
              ],
            ),
            Column(
              children: <Widget>[
                Icon(Icons.credit_card, color: Colors.red ,size: 40,),
                Text(TransferAppLocalization.of(context)!.bankkartya),
              ],
            ),
          ],
        ),
        SizedBox(height: 40,),
        ListTile(
          contentPadding: EdgeInsets.fromLTRB(30,0,30,0),
          title: Text(widget.transfer.hova),
          subtitle: Text(widget.transfer.honnan),
          isThreeLine: true,
          trailing: Text(widget.transfer.ar+" Ft"),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 250,
              child: ElevatedButton(
                onPressed: () async{
                  Reservation foglalas = await showDialog(context: context, builder: (BuildContext context) => TransferFormDialog());
                  saveReservation(foglalas);
                },
                child: Text(TransferAppLocalization.of(context)!.foglalas),
              ),
            )
          ],
        )
      ],
    );
  }

  void saveReservation(Reservation re) async{
    var _transferProvider = context.read<TransferProvider>();
    await _transferProvider.saveReservation(re);
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(backgroundColor: Colors.green.shade900,
        content: Row(children: [Icon(Icons.check, color: Colors.white,),SizedBox(width: 20,), Expanded(child: Text('Sikeres Foglalás!'),)],)
    ));
  }
}
