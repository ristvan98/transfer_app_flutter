import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/localization/Localization.dart';
import 'package:flutter_transfer_app/models/Reservation.dart';
import 'package:flutter_transfer_app/providers/TransferProvider.dart';
import 'package:flutter_transfer_app/providers/UserProvider.dart';
import 'package:provider/provider.dart';

class TransferFormDialog extends StatefulWidget {
  const TransferFormDialog({Key? key}) : super(key: key);

  @override
  _TransferFormDialogState createState() => _TransferFormDialogState();
}

class _TransferFormDialogState extends State<TransferFormDialog> {
  final formKey = GlobalKey<FormState>();

  TextEditingController _megjegyzesController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    var _transferProvider = context.read<TransferProvider>();
    var _userProvider = context.read<UserProvider>();
    return SimpleDialog(
      contentPadding: EdgeInsets.all(30),
      title: Text(TransferAppLocalization.of(context)!.foglalas),
      children: <Widget>[
        Container(
          width: 700,
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                Text(TransferAppLocalization.of(context)!.text),
                SizedBox(height: 20),
                TextFormField(
                  maxLines: null,
                  keyboardType: TextInputType.multiline,
                  controller: _megjegyzesController,
                  decoration: InputDecoration(labelText: TransferAppLocalization.of(context)!.megjegyzes),
                  validator: (value){
                    if(value != null && value.isEmpty){
                      return TransferAppLocalization.of(context)!.kotelezomezo;
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    FlatButton(
                      onPressed: (){},
                      textColor: Colors.red.shade200,
                      child: Text(TransferAppLocalization.of(context)!.megse),
                    ),
                    FlatButton(
                      onPressed: (){
                        if(formKey.currentState!.validate()){
                          Navigator.of(context).pop(Reservation(transferId: int.parse(_transferProvider.choose.id), userId: int.parse(_userProvider.LoggedUser.id), megjegyzes: _megjegyzesController.text,));
                        }
                      },
                      textColor: Colors.red,
                      child: Text(TransferAppLocalization.of(context)!.foglalas),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
