import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TransferMap extends StatefulWidget {
  final String latlang;
  const TransferMap({Key? key, required this.latlang}) : super(key: key);

  @override
  _TransferMapState createState() => _TransferMapState();
}

class _TransferMapState extends State<TransferMap> {
  Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    var split = widget.latlang.split(',');
    return Stack(
      children: [
        GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: CameraPosition(
              target: LatLng(double.parse(split[0]), double.parse(split[1])),
              zoom: 16),
          compassEnabled: false,
          myLocationButtonEnabled: false,
          scrollGesturesEnabled: false,
          tiltGesturesEnabled: false,
          zoomControlsEnabled: false,
          zoomGesturesEnabled: false,
          rotateGesturesEnabled: false,
          mapToolbarEnabled: false,
          markers: [
            Marker(
                markerId: MarkerId("1"),
                position:
                    LatLng(double.parse(split[0]), double.parse(split[1])),
                draggable: false),
          ].toSet(),
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
        ),
      ],
    );
  }
}
