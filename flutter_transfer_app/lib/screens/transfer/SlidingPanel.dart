import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/models/Transfer.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'PanelWidget.dart';
import 'TransferMap.dart';

class SlidingPanel extends StatefulWidget {
  final Transfer transfer;
  const SlidingPanel({Key? key, required this.transfer}) : super(key: key);

  @override
  _SlidingPanelState createState() => _SlidingPanelState();
}

class _SlidingPanelState extends State<SlidingPanel> {
  final _panelController = PanelController();
  @override
  Widget build(BuildContext context) {
    return SlidingUpPanel(
      controller: _panelController,
      minHeight: MediaQuery.of(context).size.height * 0.1,
      maxHeight: MediaQuery.of(context).size.height * 0.5,
      borderRadius: BorderRadius.vertical(top: Radius.circular(18)),
      parallaxEnabled: true,
      parallaxOffset: .5,
      body: TransferMap(latlang: widget.transfer.hova_latlng),
      panelBuilder: (controller) => PanelWidget(
        controller: controller,
        panelcontroller: _panelController,
        transfer: widget.transfer,
      ),
    );
  }
}
