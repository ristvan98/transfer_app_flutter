import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/localization/Localization.dart';
import 'package:flutter_transfer_app/models/User.dart';
import 'package:flutter_transfer_app/providers/NavigationProvider.dart';
import 'package:flutter_transfer_app/providers/UserProvider.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController _username;
  late TextEditingController _password;

  @override
  void initState(){
    super.initState();
    _username = TextEditingController();
    _password = TextEditingController();
  }

  void doLogin(){
    final Future<Map<String,dynamic>> data = Provider.of<UserProvider>(context, listen: false).login(_username.text, _password.text);
    data.then((response){
      if(response['status']){
        User user = response['user'];
        Provider.of<UserProvider>(context, listen: false).setLoggedUser(user);
        Provider.of<NavigationProvider>(context, listen: false).changeScreen('/home');
      }else{
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(backgroundColor: Colors.red.shade900,
            content: Row(children: [Icon(Icons.warning, color: Colors.white,),SizedBox(width: 20,), Expanded(child: Text(response['msg']),)],)
        ));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        title: Text(TransferAppLocalization.of(context)!.bejelentkezes),
        backgroundColor: Colors.redAccent.shade700,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.all(36),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                TextField(
                  controller: _username,
                  obscureText: false,
                  decoration: InputDecoration(
                    labelText: TransferAppLocalization.of(context)!.felhasznalonev,
                  ),
                ),
                SizedBox(height: 25.0),
                TextField(
                  controller: _password,
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: TransferAppLocalization.of(context)!.jelszo,
                  ),
                ),
                SizedBox(
                  height: 35.0,
                ),
                Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(30.0),
                  color: Colors.redAccent.shade700,
                  child: MaterialButton(
                    minWidth: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    onPressed: () {
                      doLogin();
                    },
                    child: Text(TransferAppLocalization.of(context)!.belepes,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white)),
                  ),
                ),
                SizedBox(
                  height: 25.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(TransferAppLocalization.of(context)!.nincsfiok),
                    SizedBox(
                      width: 5.0,
                    ),
                    InkWell(
                      onTap: () {
                        Provider.of<NavigationProvider>(context, listen: false).changeScreen("/register");
                      },
                      child: Text(TransferAppLocalization.of(context)!.regisztralj,
                          style: TextStyle(color: Colors.redAccent.shade700)),
                    )
                  ],
                ),
            ],
            ),
          ),
        ),
      ),
    );
  }
}
