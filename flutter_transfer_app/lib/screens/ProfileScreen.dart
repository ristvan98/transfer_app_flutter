import 'package:flutter/material.dart';
import 'package:flutter_transfer_app/localization/Localization.dart';
import 'package:flutter_transfer_app/providers/NavigationProvider.dart';
import 'package:flutter_transfer_app/providers/UserProvider.dart';
import 'package:flutter_transfer_app/screens/profile/ProfileForm.dart';
import 'package:flutter_transfer_app/screens/profile/ProfilePicture.dart';
import 'package:provider/provider.dart';
import '../models/User.dart';


class ProfileScreen extends StatefulWidget {

  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  late NavigationProvider navigation;
  late UserProvider userProvider;

  @override
  void initState(){
    super.initState();
    navigation = context.read<NavigationProvider>();
    userProvider = context.read<UserProvider>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(TransferAppLocalization.of(context)!.profil),
        backgroundColor: Colors.redAccent.shade700,
        shadowColor: Colors.redAccent.shade700,
        actions: [
          ButtonTheme(
            padding: EdgeInsets.all(0),
            minWidth: 30,
            child: FlatButton(
              onPressed: (){
                userProvider.setloggedInStatus(Status.LoggedOut);
                navigation.changeScreen("/");
              },
              child: Icon(Icons.logout,
                  size: 25, color: Colors.white),
            ),
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: Colors.grey.shade200,
        ),
        child: Column(
          children: <Widget>[
            ProfilePicture(img: userProvider.LoggedUser.image),
            if(userProvider.LoggedUser != null)
              Expanded(
                child: ProfileForm(user: userProvider.LoggedUser),
              )

          ],
        ),
      ),
    );
  }
}
