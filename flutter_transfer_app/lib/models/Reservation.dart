class Reservation{
  final int transferId;
  final int userId;
  final String megjegyzes;

  Reservation({required this.transferId, required this.userId, required this.megjegyzes});
}