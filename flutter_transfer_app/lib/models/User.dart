class User{
  final String id;
  final String username;
  final String password;
  final String image;

  User({required this.id, required this.username, required this.password, required this.image});

  User.fromJson(Map json): id = json['id'], username = json["username"], password = json["password"], image = json["image"];

}