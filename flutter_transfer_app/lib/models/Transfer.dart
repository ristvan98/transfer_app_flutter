class Transfer{
  final String id;
  final String honnan;
  final String hova;
  final String hova_latlng;
  final String ar;
  final String idopont;
  final String image;

  Transfer({required this.id, required this.honnan, required this.hova, required this.hova_latlng, required this.ar, required this.idopont, required this.image});

  Transfer.fromJson(Map json): id = json['id'], honnan = json['honnan'], hova = json['hova'], hova_latlng = json['hova_latlng'], ar = json['ar'], idopont = json['idopont'], image = json['image'];

}