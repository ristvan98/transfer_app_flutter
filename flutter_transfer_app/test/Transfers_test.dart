import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_transfer_app/models/Transfer.dart';
import 'package:flutter_transfer_app/providers/TransferProvider.dart';
import 'package:http/http.dart' as http;
import 'package:http/testing.dart';
import 'package:mocktail/mocktail.dart';

class MockClient extends Mock implements http.Client {}


void main(){
  group('TransferProvider', (){
    test('Vissza adja az elérhető transzferek listáját ha a http kérés sikeres',() async {
      final mock = MockClient();
      final response = [[
        {
          "id": "1",
          "honnan": "Szeged",
          "hova": "Budapest",
          "hova_latlng": "47.4814475456472, 19.10937564223248",
          "ar": "6000",
          "idopont": "2022-01-08 22:58:11",
          "image": "https://zoldkalauz.hu/files/images/921/dsc04350m.jpg"
        },
        {
          "id": "2",
          "honnan": "Szeged",
          "hova": "Székesfehérvár",
          "hova_latlng": "47.188780108436355, 18.409379086662778",
          "ar": "6000",
          "idopont": "2022-01-08 22:58:11",
          "image": "https://gsg-61ea.kxcdn.com/media/contents/Budapest%20moon%20city-4057914_1920.jpg"
        },
        {
          "id": "3",
          "honnan": "Szeged",
          "hova": "Székesfehérvár",
          "hova_latlng": "47.188780108436355, 18.409379086662778",
          "ar": "6000",
          "idopont": "2022-01-08 22:58:11",
          "image": "https://gsg-61ea.kxcdn.com/media/contents/Budapest%20moon%20city-4057914_1920.jpg"
        },
        {
          "id": "4",
          "honnan": "Szeged",
          "hova": "Székesfehérvár",
          "hova_latlng": "47.188780108436355, 18.409379086662778",
          "ar": "6000",
          "idopont": "2022-01-08 22:58:11",
          "image": "https://gsg-61ea.kxcdn.com/media/contents/Budapest%20moon%20city-4057914_1920.jpg"
        },
        {
          "id": "5",
          "honnan": "Szeged",
          "hova": "Budapest",
          "hova_latlng": "47.4814475456472, 19.10937564223248",
          "ar": "6000",
          "idopont": "2022-01-08 22:58:11",
          "image": "https://zoldkalauz.hu/files/images/921/dsc04350m.jpg"
        }
      ]];
      when(() => mock.get(Uri.parse("http://192.168.1.6/php/transfer.php"))).thenAnswer((invocation) => Future.value(http.Response(json.encode(response),200)));
        expect(await TransferProvider.loadTransfers(), isA<List<Transfer>>());
    });
  });
}